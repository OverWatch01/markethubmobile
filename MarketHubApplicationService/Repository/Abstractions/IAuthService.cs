﻿using MarketHubApplicationService.Model.Authentication;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MarketHubApplicationService.Repository.Abstractions
{
    public interface IAuthService
    {
        Task<LoginModel> Login(LoginModel loginRequest);
        Task<RegisterModel> Register(RegisterModel loginRequest);
        void Logout();
    }
}
