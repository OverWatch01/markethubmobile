﻿using MarketHubApplicationService.Model.Authentication;
using MarketHubApplicationService.Repository;
using MarketHubApplicationService.Repository.Abstractions;
using MarketHubApplicationService.Service;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

[assembly: Dependency(typeof(AuthService))]
namespace MarketHubApplicationService.Repository
{
    public class AuthService : IAuthService
    {
        HttpClient webClient = new HttpClient();

        public async Task<LoginModel> Login(LoginModel loginRequest)
        {
            LoginModel retval = null;
            var request = new HttpRequestMessage(HttpMethod.Post, $"{APIKeys.WebAPIUrl}authenticate");
            request.Content = new StringContent(JsonConvert.SerializeObject(loginRequest), Encoding.UTF8, "application/json");

            HttpResponseMessage response = await webClient.SendAsync(request);
            if (response.IsSuccessStatusCode)
            {
                var result = await response.Content.ReadAsStringAsync();
                retval = JsonConvert.DeserializeObject<LoginModel>(result);
            }
            return retval;
        }

        public async Task<RegisterModel> Register(RegisterModel model)
        {
            RegisterModel retval = null;
            var request = new HttpRequestMessage(HttpMethod.Post, $"{APIKeys.WebAPIUrl}register");
            request.Content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            HttpResponseMessage response = await webClient.SendAsync(request);
            if (response.IsSuccessStatusCode)
            {
                var result = await response.Content.ReadAsStringAsync();
                retval = JsonConvert.DeserializeObject<RegisterModel>(result);
            }
            return retval;
        }

        public void Logout()
        {
            throw new NotImplementedException();
        }
    }
}
