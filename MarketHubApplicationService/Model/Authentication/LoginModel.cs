﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MarketHubApplicationService.Model.Authentication
{
    public class LoginModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
