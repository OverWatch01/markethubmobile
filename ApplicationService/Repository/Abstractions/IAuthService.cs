﻿using ApplicationService.Model.Authentication;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationService.Repository.Abstractions
{
    public interface IAuthService
    {
        Task<LoginModel> Login(LoginModel loginRequest);
        Task<RegisterModel> Register(RegisterModel loginRequest);
        void Logout();
    }
}
