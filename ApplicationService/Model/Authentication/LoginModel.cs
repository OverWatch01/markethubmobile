﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationService.Model.Authentication
{
    /// <summary>
    /// Model for User Login.
    /// </summary>
    public class LoginModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
