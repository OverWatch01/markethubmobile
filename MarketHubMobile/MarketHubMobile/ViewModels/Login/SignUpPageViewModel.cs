﻿using MarketHubApplicationService.Repository.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace MarketHubMobile.ViewModels.Login
{
    public class SignUpPageViewModel : BaseViewModel
    {
        public ICommand LoginCommand { get; }

        IAuthService identityService;

        public string Username { get; set; }
        public string Password { get; set; }

        object authResult;

        bool loggedIn = false;
        public bool LoggedIn
        {
            get => loggedIn;
            set
            {
                SetProperty(ref loggedIn, value);
                NotLoggedIn = !LoggedIn;
            }
        }

        bool notLoggedIn = true;
        public bool NotLoggedIn { get => notLoggedIn; set => SetProperty(ref notLoggedIn, value); }


        public SignUpPageViewModel()
        {
            LoginCommand = new Command(async () => await ExecuteLoginCommand());

            identityService = DependencyService.Get<IAuthService>();
        }

        private async Task ExecuteLoginCommand()
        {
            await Application.Current.MainPage.Navigation.PopAsync();
        }
    }
}
