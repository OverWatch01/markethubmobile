﻿using MarketHubApplicationService.Model.Authentication;
using MarketHubApplicationService.Repository.Abstractions;
using MarketHubMobile.Views.Login;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace MarketHubMobile.ViewModels.Login
{
    public class LoginPageViewModel : BaseViewModel
    {
        public ICommand LoginCommand { get; }
        public ICommand SignupCommand { get; }

        IAuthService identityService;

        public string Username { get; set; }
        public string Password { get; set; }

        object authResult;

        bool loggedIn = false;
        public bool LoggedIn
        {
            get => loggedIn;
            set
            {
                SetProperty(ref loggedIn, value);
                NotLoggedIn = !LoggedIn;
            }
        }

        bool notLoggedIn = true;
        public bool NotLoggedIn { get => notLoggedIn; set => SetProperty(ref notLoggedIn, value); }


        public LoginPageViewModel()
        {
            LoginCommand = new Command(async () => await ExecuteLoginCommand());
            SignupCommand = new Command(async () => await ExecuteSignupCommand());



            identityService = DependencyService.Get<IAuthService>();
        }



        #region ExecuteLoginCommand
        protected async Task ExecuteLoginCommand()
        {
            if (IsBusy)
                return;

            if (string.IsNullOrEmpty(this.Username) || string.IsNullOrEmpty(this.Password))
                return;

            try
            {
                IsBusy = true;

                var loginRequest = new LoginModel
                {
                    Username = this.Username,
                    Password = this.Password
                };
                authResult = await identityService.Login(loginRequest);
            }
            finally
            {
                IsBusy = false;
            }

            if (authResult == null)
            {
                LoggedIn = false;
                await Application.Current.MainPage.DisplayAlert("Alert", "Error Occured", "OK");
            }
            else
            {
                LoggedIn = true;
                await Application.Current.MainPage.DisplayAlert("Alert", "Successfully Logged in", "OK");
            }
        }
        #endregion

        #region ExecuteSignupCommand
        private async Task ExecuteSignupCommand()
        {
            await Application.Current.MainPage.Navigation.PushAsync(new SignUpPage());
        } 
        #endregion
    }
}
