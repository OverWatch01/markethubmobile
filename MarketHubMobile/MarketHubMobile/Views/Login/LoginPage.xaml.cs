﻿using MarketHubMobile.ViewModels.Login;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MarketHubMobile.Views.Login
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {
            InitializeComponent();
        }
    }
}